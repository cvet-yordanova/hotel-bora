exports.isLoggedUser = function(req, res, next) {
  if (req.isAuthenticated()) {
    next();
  } else {
    req.flash("danger", "Please log in");
    res.redirect("/admin/signin");
  }
};
